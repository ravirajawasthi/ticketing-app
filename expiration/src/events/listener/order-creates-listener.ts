import { Listener, OrderCreatedEvent, Subjects } from "@byte_b/common";
import { queueGroupName } from "./queueGroupName";
import { Message } from "node-nats-streaming";
import { expirationQueue } from "../../queue/queue";

export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
  readonly subject = Subjects.OrderCreated;
  readonly queueGroupName = queueGroupName;
  async onMessage(data: OrderCreatedEvent["data"], msg: Message) {
    const delay = new Date(data.expiresAt).getTime() - new Date().getTime();
    console.log("delay = ", delay);
    await expirationQueue.add(
      {
        orderId: data.id,
      },
      { delay }
    );
    msg.ack();
  }
}
