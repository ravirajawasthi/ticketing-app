import { Subjects, Publisher, ExpirationCompleteEvent } from "@byte_b/common";
import { expirationQueue } from "../../queue/queue";

export class ExpirationCompletePublisher extends Publisher<
  ExpirationCompleteEvent
> {
  readonly subject = Subjects.ExpirationComplete;
}
