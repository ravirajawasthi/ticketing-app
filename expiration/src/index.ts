import { natsWrapper } from "./nats-wrapper";
import { OrderCreatedListener } from "./events/listener/order-creates-listener";

const start = async () => {
  if (!process.env.NATS_CLIENT_ID) {
    throw new Error("ENV: NATS_CLIENT_ID not defined!");
  }
  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error("ENV: NATS_CLUSTER_ID not defined!");
  }
  if (!process.env.NATS_URL) {
    throw new Error("ENV: NATS_URL not defined!");
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    );
    new OrderCreatedListener(natsWrapper.client).listen();
  } catch (err) {
    console.error(err);
  }
};

start();

process.on("SIGINT", () => {
  natsWrapper.client.close();
});
process.on("SIGTERM", () => {
  natsWrapper.client.close();
});
