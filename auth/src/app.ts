import express from 'express';
import 'express-async-errors';
import { currentUserRouter } from './routes/current-user';
import { signupRouter } from './routes/signup';
import { signingRouter } from './routes/signin';
import { signoutRouter } from './routes/signout';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import { NotFoundError, errorHandler } from '@byte_b/common';

const app = express();

app.set('trust proxy', true);
app.use(
  cookieSession({
    secure: false,
    signed: false
  })
)
app.use(json());
app.use(signupRouter);
app.use(currentUserRouter);
app.use(signingRouter);
app.use(signoutRouter);

app.all('*', async () => {
  throw new NotFoundError();
});

app.use(errorHandler);

export default app;
