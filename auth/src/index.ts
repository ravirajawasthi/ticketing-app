import mongoose from "mongoose";
import app from "./app";
const start = async () => {
  if (!process.env.JWT_KEY) {
    throw new Error("ENV: JWT_KEY not defined");
  }

  if (!process.env.MONGO_URI) {
    throw new Error("ENV: MONGO_URI not defined");
  }

  try {
    mongoose
      .connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      })
      .then(() => console.log("Connected to Auth-Database!"));
  } catch (err) {
    console.error(err);
  }
  app.listen(3000, () => console.log("Auth-Service listening of port 3000!"));
};

start();
