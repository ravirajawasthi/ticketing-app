import request from 'supertest';
import app from '../../app';

it('Returns a 201 after successful signup and a cookie must exist', async () => {
  const res = await request(app)
    .post('/api/users/signup')
    .send({ email: 'raman@test.com', password: '12345678' })
    .expect(201);
  expect(res.get('Set-Cookie')).toBeDefined();
});

it('400 after smaller password', async () => {
  return request(app)
    .post('/api/users/signup')
    .send({ email: 'raman@test.com', password: '12345' })
    .expect(400);
});

it('400 after password length > 24', async () => {
  return request(app)
    .post('/api/users/signup')
    .send({ email: 'raman@test.com', password: '1234512345123451234512345' })
    .expect(400);
});

it('400 after invalid email', async () => {
  return request(app)
    .post('/api/users/signup')
    .send({ email: 'ramantest.com', password: '123451234512345123451234' })
    .expect(400);
});

it('400 after missing parameters', async () => {
  request(app)
    .post('/api/users/signup')
    .send({ email: '', password: '123451234512345123451234' })
    .expect(400);
  request(app)
    .post('/api/users/signup')
    .send({ password: '123451234512345123451234' })
    .expect(400);
  request(app)
    .post('/api/users/signup')
    .send({ email: '', password: '' })
    .expect(400);
  request(app).post('/api/users/signup').send({ email: '' }).expect(400);
});

it('Duplicate emails not allowed', async () => {
  await request(app)
    .post('/api/users/signup')
    .send({ email: 'test@gmail.com', password: '1122334455' })
    .expect(201);
  await request(app)
    .post('/api/users/signup')
    .send({ email: 'test@gmail.com', password: '1122334455' })
    .expect(400);
});
