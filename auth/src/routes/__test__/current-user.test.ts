import request from 'supertest';
import app from '../../app';

it('responds with correct details about user', async () => {
  const cookie = await global.getSigninCookie();
  const res = await request(app)
    .get('/api/users/currentuser')
    .set('Cookie', cookie)
    .expect(200);
  expect(res.body.currentUser.email).toEqual(global.testEmail);
});

it('responds with null when not authenticated', async () => {
  const res = await request(app).get('/api/users/currentuser').expect(200);
  expect(res.body.currentUser).toEqual(null);
});
