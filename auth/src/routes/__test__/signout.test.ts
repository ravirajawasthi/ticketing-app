import request from 'supertest';
import app from '../../app';

it('Specific cookie after signout', async () => {
  await request(app)
    .post('/api/users/signup')
    .send({ email: 'demo@test.com', password: 'passwords' })
    .expect(201);
  const res = await request(app).post('/api/users/signout').expect(200);
  expect(res.get('Set-Cookie')).toBeDefined();
});
