import request from "supertest";
import app from "../../app";

it("fails when email does not exist", async () => {
  await request(app)
    .post("/api/users/signin")
    .send({
      email: "randomEmail@googlefacebookmicrosoft.com",
      password: "12345678",
    })
    .expect(400);
});

it("fails when email or password are not provided", async () => {
  await request(app).post("/api/users/signin").send({}).expect(400);

  await request(app)
    .post("/api/users/signin")
    .send({
      email: "",
    })
    .expect(400);
  await request(app)
    .post("/api/users/signin")
    .send({
      password: "1",
    })
    .expect(400);

  await request(app)
    .post("/api/users/signin")
    .send({
      email: "oahdaosjhd@oshdpoadh",
      password: "121212121212",
    })
    .expect(400);
});

it("fails when incorrect password is supplied", async () => {
  await request(app)
    .post("/api/users/signup")
    .send({ email: "demo@test.com", password: "123454567" })
    .expect(201);

  await request(app)
    .post("/api/users/signin")
    .send({
      email: "demo@test.com",
      password: "i dunno the password",
    })
    .expect(400);
});

it("Should successfully login when provided with correct credentials and provide a cookie", async () => {
  await request(app)
    .post("/api/users/signup")
    .send({ email: "demo@test.com", password: "passwords" })
    .expect(201);

  const res = await request(app)
    .post("/api/users/signin")
    .send({
      email: "demo@test.com",
      password: "passwords",
    })
    .expect(200);
  expect(res.get("Set-Cookie")).toBeDefined();
});
