import mongoose, { Mongoose } from "mongoose";
import { Password } from "../helpers/password";

interface userAttributes {
  email: string;
  password: string;
}

interface UserModel extends mongoose.Model<UserDoc> {
  build(attr: userAttributes): UserDoc;
}

interface UserDoc extends mongoose.Document {
  email: string;
  createdAt: string;
  updatedAt: string;
  password: string;
}

const userSchema = new mongoose.Schema(
  {
    email: { type: String, required: true },
    password: { type: String, required: true },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
      },
    },
  }
);

//Arrow function would have wrong value of 'this'
//when we are finished with middleware code, we call done to let mongoose proceed
userSchema.pre("save", async function (done) {
  if (this.isModified("password")) {
    const hashedPassword = await Password.toHash(this.get("password"));
    this.set("password", hashedPassword);
  }
  done();
});

userSchema.statics.build = (attr: userAttributes) => {
  return new User(attr);
};

const User = mongoose.model<UserDoc, UserModel>("User", userSchema);

const user = User.build({
  email: "dasd",
  password: "asdasd",
});

export { User };
