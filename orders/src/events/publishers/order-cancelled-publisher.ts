import { Publisher, OrderCancelledEvent, Subjects } from "@byte_b/common";

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
  readonly subject = Subjects.OrderCancelled;
}
