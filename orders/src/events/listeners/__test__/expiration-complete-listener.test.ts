import mongoose from "mongoose";
import { natsWrapper } from "../../../nats-wrapper";
import { ExpirationCompleteListener } from "../expiration-complete-listener";
import { Ticket } from "../../../models/ticket";
import { Order, OrderStatus } from "../../../models/orders";
import { ExpirationCompleteEvent } from "@byte_b/common";
import { Message } from "node-nats-streaming";

const createFakeEvent = async () => {
  const listener = new ExpirationCompleteListener(natsWrapper.client);
  const ticket = Ticket.build({
    id: mongoose.Types.ObjectId().toHexString(),
    title: "lady gaga",
    price: 84,
  });
  await ticket.save();
  const order = Order.build({
    expiresAt: new Date(),
    status: OrderStatus.Created,
    ticket: ticket,
    userId: mongoose.Types.ObjectId().toHexString(),
  });
  await order.save();
  const data: ExpirationCompleteEvent["data"] = { orderId: order.id };
  //@ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  };
  return { listener, data, order, ticket, msg };
};

it("Order status is updated to cancelled", async () => {
  const { listener, data, order, msg } = await createFakeEvent();
  await listener.onMessage(data, msg);
  const fetchedOrder = await Order.findById(order.id);
  expect(fetchedOrder?.status).toEqual(OrderStatus.Cancelled);
});
it("A event is emitted for order cancelled", async () => {
  const { listener, data, order, msg } = await createFakeEvent();
  await listener.onMessage(data, msg);

  const passedArgumentsToEvent = JSON.parse(
    (natsWrapper.client.publish as jest.Mock).mock.calls[0][1]
  );
  const fetchedOrder = await Order.findById(order.id).populate("ticket");
  expect(natsWrapper.client.publish).toHaveBeenCalled();
  expect(passedArgumentsToEvent.id).toEqual(fetchedOrder?.id);
  expect(passedArgumentsToEvent.ticket.id).toEqual(fetchedOrder?.ticket.id);
  expect(passedArgumentsToEvent.status).toEqual(OrderStatus.Cancelled);
  expect(fetchedOrder?.status).toEqual(OrderStatus.Cancelled);
});
it("Message is successfully acknowledged", async () => {
  const { listener, data, order, msg } = await createFakeEvent();
  await listener.onMessage(data, msg);
  expect(msg.ack).toHaveBeenCalled();
});
