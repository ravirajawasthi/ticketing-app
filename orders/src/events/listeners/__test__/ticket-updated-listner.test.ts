import mongoose, { version } from "mongoose";
import { natsWrapper } from "../../../nats-wrapper";
import { Message } from "node-nats-streaming";
import { Ticket } from "../../../models/ticket";
import { TicketUpdatedListener } from "../ticket-update-listener";
import { TicketUpdatedEvent } from "@byte_b/common";

const createFakeEvent = async () => {
  const listener = new TicketUpdatedListener(natsWrapper.client);

  const ticket = Ticket.build({
    id: mongoose.Types.ObjectId().toHexString(),
    price: 565,
    title: "lady gaga conceert",
  });

  await ticket.save();

  const data: TicketUpdatedEvent["data"] = {
    id: ticket.id,
    price: ticket.price,
    title: ticket.title,
    userId: mongoose.Types.ObjectId().toHexString(),
    version: ticket.version + 1,
  };

  //@ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  };

  return { msg, ticket, data, listener };
};

it("Creates, Saves and updates a given ticket", async () => {
  const { msg, ticket, data, listener } = await createFakeEvent();
  await listener.onMessage(data, msg);
  const updatedTicket = await Ticket.findById(ticket.id);
  expect(updatedTicket?.title).toEqual(ticket.title);
  expect(updatedTicket?.id).toEqual(ticket.id);
  expect(updatedTicket?.price).toEqual(ticket.price);
  expect(updatedTicket?.version).toEqual(ticket.version + 1);
  expect(updatedTicket?.version).toEqual(data.version);
});

it("acknowledges message", async () => {
  const { msg, ticket, data, listener } = await createFakeEvent();
  await listener.onMessage(data, msg);
  expect(msg.ack).toHaveBeenCalled();
});

it("Handles out of order events", async () => {
  const { msg, ticket, listener, data } = await createFakeEvent();
  data.version = 55;
  try {
    await listener.onMessage(data, msg);
  } catch (err) {}
  expect(msg.ack).not.toHaveBeenCalled();
});
