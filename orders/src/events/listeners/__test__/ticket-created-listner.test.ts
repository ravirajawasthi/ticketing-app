import mongoose from "mongoose";
import { TicketCreatedListner } from "../ticket-created-listener";
import { natsWrapper } from "../../../nats-wrapper";
import { TicketCreatedEvent } from "@byte_b/common";
import { Message } from "node-nats-streaming";
import { Ticket } from "../../../models/ticket";

const createFakeEvent = async () => {
  const listner = new TicketCreatedListner(natsWrapper.client);
  const data: TicketCreatedEvent["data"] = {
    version: 0,
    id: new mongoose.Types.ObjectId().toString(),
    title: "lady gaga concert",
    price: 778,
    userId: new mongoose.Types.ObjectId().toString(),
  };
  //@ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  };

  return { listner, data, msg };
};

it("creates and saved a ticket", async () => {
  const { listner, data, msg } = await createFakeEvent();
  await listner.onMessage(data, msg);
  const ticket = await Ticket.findById(data.id);

  expect(ticket).toBeDefined();
  expect(ticket?.title).toEqual(data.title);
  expect(ticket?.price).toEqual(data.price);
});

it("Acknowledges nats streaming server message", async () => {
  const { listner, data, msg } = await createFakeEvent();
  await listner.onMessage(data, msg);
  expect(msg.ack).toHaveBeenCalled();
});
