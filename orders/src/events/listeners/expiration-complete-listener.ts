import {
  Subjects,
  Listener,
  ExpirationCompleteEvent,
  OrderStatus,
} from "@byte_b/common";
import { queueGroupName } from "./queue-group-name";
import { Message } from "node-nats-streaming";
import { Order } from "../../models/orders";
import { OrderCancelledPublisher } from "../publishers/order-cancelled-publisher";

export class ExpirationCompleteListener extends Listener<
  ExpirationCompleteEvent
> {
  readonly subject = Subjects.ExpirationComplete;
  queueGroupName = queueGroupName;
  async onMessage(data: ExpirationCompleteEvent["data"], msg: Message) {
    const order = await Order.findById(data.orderId).populate("ticket");
    if (!order) {
      throw new Error("Order not found!");
    }
    if (order.status !== OrderStatus.Completed) {
      order.set({ status: OrderStatus.Cancelled });
      await order.save();
      new OrderCancelledPublisher(this.client).publish({
        id: order.id,
        status: order.status,
        userId: order.userId,
        expiresAt: order.expiresAt.toISOString(),
        version: order.version,
        ticket: {
          id: order.ticket.id,
          price: order.ticket.price,
        },
      });
    }
    msg.ack();
  }
}
