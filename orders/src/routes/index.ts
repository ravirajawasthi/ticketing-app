import express from "express";
import { requireAuth } from "@byte_b/common";
import { Order } from "../models/orders";

const router = express.Router();

router.get("/api/orders", async (req, res) => {
  const orders = await Order.find({ userId: req.currentUser!.id }).populate(
    "ticket"
  );
  res.send(orders);
});

export { router as indexOrderRouter };
