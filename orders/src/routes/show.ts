import express from "express";
import mongoose from "mongoose";
import {
  requireAuth,
  BadRequestError,
  NotFoundError,
  currentUser,
  NotAuthorizedError,
} from "@byte_b/common";
import { Order } from "../models/orders";
const router = express.Router();

router.get("/api/orders/:orderId", requireAuth, async (req, res) => {
  const orderId = req.params.orderId;
  const isObjectId = mongoose.Types.ObjectId.isValid(orderId);
  if (!isObjectId) {
    throw new BadRequestError("Invalid Object Id");
  }

  const order = await Order.findById(orderId).populate("ticket");
  if (!order) {
    throw new NotFoundError("Order not found!");
  }
  if (order.userId !== req.currentUser!.id) {
    throw new NotAuthorizedError();
  }
  res.send(order);
});

export { router as showOrderRouter };
