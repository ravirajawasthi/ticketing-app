import express from "express";
import mongoose from "mongoose";
import {
  requireAuth,
  BadRequestError,
  NotFoundError,
  NotAuthorizedError,
} from "@byte_b/common";
import { Order, OrderStatus } from "../models/orders";
import { OrderCancelledPublisher } from "../events/publishers/order-cancelled-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.delete("/api/orders/:orderId", requireAuth, async (req, res) => {
  const orderId = req.params.orderId;
  const isValidMongooseId = mongoose.Types.ObjectId.isValid(orderId);
  if (!isValidMongooseId) {
    throw new BadRequestError("Invalid Order ID");
  }
  const fetchedOrder = await Order.findById(orderId).populate("ticket");
  if (!fetchedOrder) {
    throw new NotFoundError("Order not Found!");
  }
  if (fetchedOrder.userId !== req.currentUser!.id) {
    throw new NotAuthorizedError();
  }
  fetchedOrder.status = OrderStatus.Cancelled;
  await fetchedOrder.save();

  new OrderCancelledPublisher(natsWrapper.client).publish({
    id: fetchedOrder.id,
    status: fetchedOrder.status,
    userId: fetchedOrder.userId,
    expiresAt: fetchedOrder.expiresAt.toISOString(),
    version: fetchedOrder.version,
    ticket: {
      id: fetchedOrder.ticket.id,
      price: fetchedOrder.ticket.price,
    },
  });
  res.status(204).send(fetchedOrder);
});

export { router as deleteOrderRouter };
