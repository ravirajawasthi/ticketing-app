import request from "supertest";
import mongoose from "mongoose";
import app from "../../app";
import { Order } from "../../models/orders";
import { Ticket } from "../../models/ticket";
import { requireAuth } from "@byte_b/common";

const createTicket = async (title = "concert") => {
  const ticket = Ticket.build({
    title: title,
    price: 20,
    id: new mongoose.Types.ObjectId().toHexString(),
  });
  await ticket.save();
  return ticket;
};

it("fetches order for a particular user", async () => {
  const ticketOne = await createTicket();
  const ticketTwo = await createTicket();
  const ticketThree = await createTicket();

  const userOne = global.getFakeSigninCookie();
  const userTwo = global.getFakeSigninCookie();

  // Orders by user 1
  const { body: orderOne } = await request(app)
    .post("/api/orders")
    .set("Cookie", userOne)
    .send({ ticketId: ticketOne.id })
    .expect(201);

  // Orders by user 2
  const { body: orderTwo } = await request(app)
    .post("/api/orders")
    .set("Cookie", userTwo)
    .send({ ticketId: ticketTwo.id })
    .expect(201); // Orders by user 1
  const { body: orderThree } = await request(app)
    .post("/api/orders")
    .set("Cookie", userTwo)
    .send({ ticketId: ticketThree.id })
    .expect(201);

  //get order by user 1
  let response = await request(app)
    .get("/api/orders")
    .set("Cookie", userOne)
    .send();
  expect(response.body.length).toEqual(1);
  expect(response.body[0].id === orderOne.id);

  //get order by user 2
  response = await request(app)
    .get("/api/orders")
    .set("Cookie", userTwo)
    .send();
  response = await request(app)
    .get("/api/orders")
    .set("Cookie", userTwo)
    .send();
  expect(response.body.length).toEqual(2);
  expect(response.body[0].id === orderTwo.id);
  expect(response.body[1].id === orderThree.id);
});
