import request from "supertest";
import mongoose from "mongoose";
import app from "../../app";
import { Order, OrderStatus } from "../../models/orders";
import { Ticket } from "../../models/ticket";
import { natsWrapper } from "../../nats-wrapper";

it("Returns an error if invalid ticket id is provided", async () => {
  const ticketId = mongoose.Types.ObjectId();

  await request(app)
    .post("/api/orders")
    .set("Cookie", global.getFakeSigninCookie())
    .send({ ticketId })
    .expect(404);
});

it("Error if ticket is already reserved", async () => {
  const ticket = Ticket.build({
    title: "concert",
    price: 20,
    id: new mongoose.Types.ObjectId().toHexString(),
  });
  await ticket.save();

  const order = Order.build({
    ticket,
    userId: "asdfgjbhiklnosdfghuosdfjio",
    status: OrderStatus.Created,
    expiresAt: new Date(),
  });
  await order.save();

  await request(app)
    .post("/api/orders")
    .set("Cookie", global.getFakeSigninCookie())
    .send()
    .expect(400);
});

it("Successfully reserve a Ticket", async () => {
  const ticket = Ticket.build({
    title: "concert",
    price: 20,
    id: new mongoose.Types.ObjectId().toHexString(),
  });
  await ticket.save();
  await request(app)
    .post("/api/orders")
    .set("Cookie", global.getFakeSigninCookie())
    .send({ ticketId: ticket.id })
    .expect(201);
});

it("Verify emitted event", async () => {
  const ticket = Ticket.build({
    title: "concert",
    id: new mongoose.Types.ObjectId().toHexString(),
    price: 20,
  });
  await ticket.save();
  const user = global.getFakeSigninCookie();

  const { body: order } = await request(app)
    .post("/api/orders")
    .set("Cookie", user)
    .send({ ticketId: ticket.id })
    .expect(201);
  expect(natsWrapper.client.publish).toHaveBeenCalled();
});
