import nats, { Stan } from 'node-nats-streaming'
class NatsWrapper {
    private _client?: Stan;
    get client() {
        if (!this._client) {
            throw new Error("Cannot get client before invoking connect function")
        }
        return this._client
    }
    connect(clusterId: string, clientId: string, url: string) {
        this._client = nats.connect(clusterId, clientId, { url })
        this.client.on('close', () => {
            console.log('NATS connection closed : Ticketing service');
            process.exit();
        });
        return new Promise((resolve, reject) => {
            this.client.on('connect', () => {
                console.log('Connected to NATS-streaming-server')
                resolve()
            });
            this.client.on('error', (error) => {
                reject(error)
            })
        })
    }
    close() {

    }
}

export const natsWrapper = new NatsWrapper();