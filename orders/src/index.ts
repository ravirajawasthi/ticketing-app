import mongoose from "mongoose";
import app from "./app";
import { natsWrapper } from "./nats-wrapper";
import { TicketCreatedListner } from "./events/listeners/ticket-created-listener";
import { TicketUpdatedListener } from "./events/listeners/ticket-update-listener";
import { ExpirationCompleteListener } from "./events/listeners/expiration-complete-listener";
import { PaymentCreatedListener } from "./events/listeners/payment-created-listener";

const start = async () => {
  if (!process.env.JWT_KEY) {
    throw new Error("ENV: JWT_KEY not defined!");
  }
  if (!process.env.MONGO_URI) {
    throw new Error("ENV: MONGO_URI not defined!");
  }
  if (!process.env.NATS_CLIENT_ID) {
    throw new Error("ENV: NATS_CLIENT_ID not defined!");
  }
  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error("ENV: NATS_CLUSTER_ID not defined!");
  }
  if (!process.env.NATS_URL) {
    throw new Error("ENV: NATS_URL not defined!");
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    );

    new TicketCreatedListner(natsWrapper.client).listen();
    new TicketUpdatedListener(natsWrapper.client).listen();
    new ExpirationCompleteListener(natsWrapper.client).listen();
    new PaymentCreatedListener(natsWrapper.client).listen();

    await mongoose
      .connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      })
      .then(() => console.log("Connected to tickets-Database!!"));
  } catch (err) {
    console.error(err);
  }
  app.listen(3000, () =>
    console.log("Tickets-Service listening of port 3000!!")
  );
};

start();

process.on("SIGINT", () => {
  natsWrapper.client.close();
});
process.on("SIGTERM", () => {
  natsWrapper.client.close();
});
