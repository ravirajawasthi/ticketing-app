import express from "express";
import "express-async-errors";
import cookieSession from "cookie-session";
import { json } from "body-parser";

//Routes
import { createTicketRouter } from "./routes/new";
import { getTicketRouter } from "./routes/show";
import { indexTicketRouter } from "./routes/index";
import { updateTicketRouter } from "./routes/update";

//Middleware and errors
import { NotFoundError, errorHandler, currentUser } from "@byte_b/common";

//Creating App
const app = express();

//~~~~~~~~~~~~~~~~Configuring App~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Express-session config
app.set("trust proxy", true); //true because our app gets traffic via ingree-iginx
app.use(
  cookieSession({
    secure: false,
    signed: false,
  })
);
app.use(json()); //this is so we get req.body as json

//~~~~~~~~~~~~~~~~Routes~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
app.use(currentUser);
app.use(createTicketRouter);
app.use(getTicketRouter);
app.use(indexTicketRouter);
app.use(updateTicketRouter);

//This line should always be after all routes
app.all("*", async () => {
  throw new NotFoundError();
});

app.use(errorHandler);

export default app;
