import { Ticket } from "../tickets";
import mongoose from "mongoose";
it("test optimistic concurrency control", async (done) => {
  const ticket = Ticket.build({
    title: "lady gaga concert",
    price: 20,
    userId: new mongoose.Types.ObjectId().toString(),
  });

  await ticket.save();

  const firstTicket = await Ticket.findById(ticket.id);
  const secondTicket = await Ticket.findById(ticket.id);

  firstTicket?.set({ price: 10 });
  firstTicket?.set({ price: 15 });

  await firstTicket?.save();

  try {
    await secondTicket?.save();
  } catch (err) {
    return done();
  }
  throw new Error("Test failed!");
});

it("increments the version number on multiple saves", async () => {
  const ticket = Ticket.build({
    title: "concert",
    price: 20,
    userId: new mongoose.Types.ObjectId().toString(),
  });

  await ticket.save();
  expect(ticket.version).toEqual(0);
  await ticket.save();
  expect(ticket.version).toEqual(1);
  await ticket.save();
  expect(ticket.version).toEqual(2);
});
