import express, { Request, Response } from "express";
import { Ticket } from "../models/tickets";
import { NotFoundError, BadRequestError } from "@byte_b/common";
import { Types } from "mongoose";

const router = express.Router();

router.get("/api/tickets/:ticketId", async (req, res) => {
  //Check if ticketId is a mongoose ObjectId
  const ObjectId = Types.ObjectId;
  const ticketId = req.params.ticketId;
  const ObjectIdCast = new ObjectId(ticketId);
  if (ObjectIdCast.equals(ticketId)) {
    //Check if there exists a ticket with given id
    const foundTicket = await Ticket.findById(ticketId);
    if (!foundTicket) throw new NotFoundError("Ticket not found");
    else res.send(foundTicket);
  } else throw new BadRequestError("Invalid ticketId provided");
});

export { router as getTicketRouter };
