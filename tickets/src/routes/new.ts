import express, { Request, Response } from "express";
import { Ticket } from "../models/tickets";
import { body } from "express-validator";
import { requireAuth, validateRequest } from "@byte_b/common";
import { TicketCreatedPublisher } from "../events/publisher/ticket-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/tickets",
  requireAuth,
  [
    body("title").not().isEmpty().withMessage("Title is required"),
    body("price").notEmpty().withMessage("Invalid Price"),
    body("price")
      .isFloat({ gt: 0 })
      .withMessage("Price must be greater than 0"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { title, price } = req.body;
    const userId = req.currentUser!.id; //requireAuth middleware guarantees that req.currentUser exists
    const newTicket = Ticket.build({ title, price, userId });
    await newTicket.save();
    new TicketCreatedPublisher(natsWrapper.client).publish({
      id: newTicket.id,
      title: newTicket.title,
      price: newTicket.price,
      userId: newTicket.userId,
      version: newTicket.version,
    });

    res.status(201).send(newTicket);
  }
);

export { router as createTicketRouter };
