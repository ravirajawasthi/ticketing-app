import app from "../../app";
import request from "supertest";
import mongoose from "mongoose";
import { Types } from "mongoose";
import { natsWrapper } from "../../nats-wrapper";
import { Ticket } from "../../models/tickets";

const getFakeObjectId = () => new Types.ObjectId().toHexString();

it("Returns a 404 if provided ticket id is not real", async () => {
  await request(app)
    .put(`/api/tickets/${getFakeObjectId()}`)
    .send({ title: "updated title", price: "232" })
    .set("Cookie", global.getFakeSigninCookie())
    .expect(404);
});
it("Returns a 401 if user is not authenticated", async () => {
  await request(app)
    .put(`/api/tickets/${getFakeObjectId()}`)
    .send({ title: "updated title", price: "232" })
    .expect(401);
});
it("Returns a 401 if user does not own the ticket", async () => {
  //get 2 fake user tokens
  const userA = global.getFakeSigninCookie(
    "spdkfjaspofjaspfj",
    "aojhdao@apoijfapf.com"
  );
  const userB = global.getFakeSigninCookie(
    "spdkfjaspofjaspfj",
    "aojhdao@apoijfapf.com"
  );

  //userA will create a ticket first
  const createdTicket = await request(app)
    .post("/api/tickets")
    .send({ title: "concert", price: 454 })
    .set("Cookie", userA)
    .expect(201);
  //userB will try to modify ticket using id retreived
  request(app)
    .put(`/api/tickets/${createdTicket.body.id}`)
    .send({ title: "updated concert ticket", price: 1 })
    .set("Cookie", userB)
    .expect(401);
  //again get userA ticket from api and make sure update hasn't been processed
  const ticket = await request(app).get(
    `/api/tickets/${createdTicket.body.id}`
  );
  expect(ticket.body.title).toEqual("concert");
  expect(ticket.body.price).toEqual(454);
});

it("Returns a 400 if user provided invalid title or price", async () => {
  const authCookie = global.getFakeSigninCookie();
  const createdTicket = await request(app)
    .post("/api/tickets")
    .send({ title: "concert", price: 454 })
    .set("Cookie", authCookie)
    .expect(201);
  await request(app)
    .put(`/api/tickets/${createdTicket.body.id}`)
    .send({
      title: "",
    })
    .set("Cookie", authCookie)
    .expect(400);

  await request(app)
    .put(`/api/tickets/${createdTicket.body.id}`)
    .send({
      price: -2,
    })
    .set("Cookie", authCookie)
    .expect(400);

  await request(app)
    .put(`/api/tickets/${createdTicket.body.id}`)
    .send({})
    .set("Cookie", authCookie)
    .expect(400);
});

it("updated the ticket only when valid input is provided", async () => {
  //Creating a ticket
  const authCookie = global.getFakeSigninCookie();
  const createdTicket = await request(app)
    .post("/api/tickets")
    .send({ title: "concert", price: 454 })
    .set("Cookie", authCookie)
    .expect(201);

  //Updating ticket with same credentials
  await request(app)
    .put(`/api/tickets/${createdTicket.body.id}`)
    .send({ title: "updated concert", price: 545 })
    .set("Cookie", authCookie)
    .expect(200);
  //The ticket must have updated valud
  const updatedTicket = await request(app)
    .get(`/api/tickets/${createdTicket.body.id}`)
    .expect(200);
  expect(updatedTicket.body.title).toEqual("updated concert");
  expect(updatedTicket.body.price).toEqual(545);
});

it("publishes an event", async () => {
  //Creating a ticket
  const authCookie = global.getFakeSigninCookie();
  const createdTicket = await request(app)
    .post("/api/tickets")
    .send({ title: "concert", price: 454 })
    .set("Cookie", authCookie)
    .expect(201);

  //Updating ticket with same credentials
  await request(app)
    .put(`/api/tickets/${createdTicket.body.id}`)
    .send({ title: "updated concert", price: 545 })
    .set("Cookie", authCookie)
    .expect(200);
  //The ticket must have updated valud
  const updatedTicket = await request(app)
    .get(`/api/tickets/${createdTicket.body.id}`)
    .expect(200);
  expect(updatedTicket.body.title).toEqual("updated concert");
  expect(updatedTicket.body.price).toEqual(545);

  expect(natsWrapper.client.publish).toHaveBeenCalled();
});

it("Decline update to a reserved Ticket", async () => {
  const authCookie = global.getFakeSigninCookie();
  const createdTicket = await request(app)
    .post("/api/tickets")
    .send({ title: "concert", price: 454 })
    .set("Cookie", authCookie)
    .expect(201);
  const foundTicket = await Ticket.findById(createdTicket.body.id);
  foundTicket!.set({ orderId: new mongoose.Types.ObjectId().toHexString() });
  await foundTicket!.save();
  //Updating ticket with same credentials
  await request(app)
    .put(`/api/tickets/${createdTicket.body.id}`)
    .send({ title: "updated concert", price: 545 })
    .set("Cookie", authCookie)
    .expect(400);
});
