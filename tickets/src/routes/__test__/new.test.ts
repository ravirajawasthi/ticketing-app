import request from "supertest";
import app from "../../app";
import { Ticket } from "../../models/tickets";
import { natsWrapper } from "../../nats-wrapper";

it("has a route handles listening to /api/tickets for post requests", async () => {
  const response = await request(app).post("/api/tickets").send({});
  expect(response.status).not.toEqual(404);
});

it("Only a signed in user can access, should get a 401 response", async () => {
  const response = await request(app).post("/api/tickets").send({});
  expect(response.status).toEqual(401);
});

it("returns status other than 401 when user is logged in", async () => {
  const response = await request(app)
    .post("/api/tickets")
    .set("Cookie", global.getFakeSigninCookie())
    .send({});
  expect(response.status).not.toEqual(401);
});

it("Error if invalid ticket title is provided", async () => {
  let response = await request(app)
    .post("/api/tickets")
    .set("Cookie", global.getFakeSigninCookie())
    .send({
      title: "",
      price: 10,
    });
  expect(response.status).toEqual(400);
  response = await request(app)
    .post("/api/tickets")
    .set("Cookie", global.getFakeSigninCookie())
    .send({
      price: 10,
    });
  expect(response.status).toEqual(400);
});
it("Error if invalid ticket price is provided", async () => {
  let response = await request(app)
    .post("/api/tickets")
    .set("Cookie", global.getFakeSigninCookie())
    .send({
      title: "ssd",
      price: -10,
    });
  expect(response.status).toEqual(400);
  response = await request(app)
    .post("/api/tickets")
    .set("Cookie", global.getFakeSigninCookie())
    .send({
      title: "random",
    });
  expect(response.status).toEqual(400);
});
it("Creates ticket with valid input", async () => {
  const fakeTicketTitle = "ramdom Ticket title🤦‍♂️";
  const fakeTicketPrice = Math.random() * 200 + 1;

  //Before every new test our database is completely empty
  let tickets = await Ticket.find({});
  expect(tickets.length).toEqual(0);
  //create a new ticket
  await request(app)
    .post("/api/tickets")
    .set("Cookie", global.getFakeSigninCookie())
    .send({ price: fakeTicketPrice, title: fakeTicketTitle })
    .expect(201);

  //get all tickets
  tickets = await Ticket.find({});
  //verifu whether a ticket was created
  expect(tickets.length).toEqual(1);
  //check if it is the same ticket that we created
  const createdTicket = tickets[0];
  expect(createdTicket.price).toEqual(fakeTicketPrice);
  expect(createdTicket.title).toEqual(fakeTicketTitle);
});

it("publishes an event", async () => {
  const fakeTicketTitle = "ramdom Ticket title🤦‍♂️";
  const fakeTicketPrice = Math.random() * 200 + 1;
  await request(app)
    .post("/api/tickets")
    .set("Cookie", global.getFakeSigninCookie())
    .send({ price: fakeTicketPrice, title: fakeTicketTitle })
    .expect(201);

  expect(natsWrapper.client.publish).toHaveBeenCalled();
});
