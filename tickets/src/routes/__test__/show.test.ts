import request from "supertest";
import app from "../../app";
import { Types } from "mongoose";

it("gets a ticket from the database", async () => {
  //create a fake ticket and then try to get it using api
  const fakeTicketTitle = "ramdom Ticket title🤦‍♂️";
  const fakeTicketPrice = Math.random() * 200 + 1;
  //we are sending complete ticket back from server
  const response = await request(app)
    .post("/api/tickets")
    .send({ title: fakeTicketTitle, price: fakeTicketPrice })
    .set("Cookie", global.getFakeSigninCookie())
    .expect(201);
  //we query ticket using id, we expect a 200 response
  const getTicketResponse = await request(app)
    .get(`/api/tickets/${response.body.id}`)
    .set("Cookie", global.getFakeSigninCookie())
    .expect(200);

  //check the value of created ticket to queried ticket
  expect(getTicketResponse.body.id).toEqual(response.body.id);
  expect(getTicketResponse.body.title).toEqual(fakeTicketTitle);
  expect(getTicketResponse.body.price).toEqual(fakeTicketPrice);
});

it("404 error when a ticket is not found", async () => {
  const fakeObjectId = Types.ObjectId().toHexString();
  request(app).get(`/api/tickets/${fakeObjectId}`).expect(404);
});
