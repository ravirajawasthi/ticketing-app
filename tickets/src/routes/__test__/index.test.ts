import request from "supertest";
import app from "../../app";
import { response } from "express";

const createTicket = () => {
  return request(app)
    .post("/api/tickets")
    .send({
      title: "sdjlsdfjlh",
      price: 21,
    })
    .set("Cookie", global.getFakeSigninCookie());
};

it("Fetch all tickets in a database", async () => {
  await createTicket();
  await createTicket();
  await createTicket();
  const tickets = await request(app).get("/api/tickets").expect(200);
  expect(tickets.body.length).toEqual(3);
});
