import express, { Request, Response } from "express";
import { Types } from "mongoose";
import { body } from "express-validator";
import { Ticket } from "../models/tickets";
import { natsWrapper } from "../nats-wrapper";
import { TicketUpdatedPublisher } from "../events/publisher/ticket-update-publisher";
import {
  NotFoundError,
  validateRequest,
  requireAuth,
  NotAuthorizedError,
  BadRequestError,
} from "@byte_b/common";

const router = express.Router();

router.put(
  "/api/tickets/:ticketId",
  requireAuth,
  [
    body("title").not().isEmpty().withMessage("Title is required"),
    body("price").notEmpty().withMessage("Invalid Price"),
    body("price")
      .isFloat({ gt: 0 })
      .withMessage("Price must be greater than 0"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    //veryfying ticketId is a mongoose objectId
    const ticketId = req.params.ticketId;
    const isObjectId = new Types.ObjectId(ticketId).toHexString() === ticketId;
    if (isObjectId) {
      //getting ticket from database
      const foundTicket = await Ticket.findById(ticketId);
      //handling conditions
      if (!foundTicket) throw new NotFoundError("Ticket not found");
      if (foundTicket.orderId) throw new BadRequestError("Ticket is reserved");
      //user requesting update should be same as who created the ticket
      if (foundTicket.userId != req.currentUser!.id) {
        //intentionally using == because both have different types. Weird!!
        throw new NotAuthorizedError();
      } else {
        //updating ticket and sending response
        foundTicket.set({
          title: req.body.title,
          price: req.body.price,
        });
        await foundTicket.save();
        new TicketUpdatedPublisher(natsWrapper.client).publish({
          id: foundTicket.id,
          title: foundTicket.title,
          userId: foundTicket.userId,
          price: foundTicket.price,
          version: foundTicket.version,
          orderId: foundTicket.orderId,
        });
        res.send(foundTicket);
      }
    } else throw new BadRequestError("Invalid Ticket id");
  }
);

export { router as updateTicketRouter };
