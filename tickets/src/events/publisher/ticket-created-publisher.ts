import { Publisher, Subjects, TicketCreatedEvent } from "@byte_b/common";

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent>{
    readonly subject  = Subjects.TicketCreated 
}

