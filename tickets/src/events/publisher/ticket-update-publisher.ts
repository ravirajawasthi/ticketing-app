import { Publisher, Subjects, TicketUpdatedEvent } from "@byte_b/common";

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent>{
    readonly subject = Subjects.TicketUpdated
}

