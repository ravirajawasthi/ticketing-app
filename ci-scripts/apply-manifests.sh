#installing doctl
doctl auth init -t $DIGITALOCEAN_TOKEN
doctl kubernetes cluster kubeconfig save ticketing

kubectl apply -f infra/k8s/*
kubectl apply -f infra/k8s-prod/*