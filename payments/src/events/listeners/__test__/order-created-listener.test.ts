import mongoose from "mongoose";
import { OrderCreatedListener } from "../order-created-listener";
import { natsWrapper } from "../../../nats-wrapper";
import { OrderCreatedEvent, OrderStatus } from "@byte_b/common";
import { Order } from "../../../models/orders";
import { Message } from "node-nats-streaming";

const createFakeEvent = async () => {
  const listener = new OrderCreatedListener(natsWrapper.client);
  const data: OrderCreatedEvent["data"] = {
    id: mongoose.Types.ObjectId().toHexString(),
    version: 0,
    status: OrderStatus.Created,
    expiresAt: new Date().toISOString(),
    userId: mongoose.Types.ObjectId().toHexString(),
    ticket: {
      id: mongoose.Types.ObjectId().toHexString(),
      price: 74,
    },
  };
  //@ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  };

  return { listener, data, msg };
};

it("replicates order information in its own order collection", async () => {
  const { listener, data, msg } = await createFakeEvent();
  await listener.onMessage(data, msg);
  const fetchedOrder = await Order.findById(data.id);
  expect(fetchedOrder!.id).toEqual(data.id);
  expect(fetchedOrder!.price).toEqual(data.ticket.price);
});
it("Acknowledges message", async () => {
  const { listener, data, msg } = await createFakeEvent();
  await listener.onMessage(data, msg);
  expect(msg.ack).toHaveBeenCalled();
});
