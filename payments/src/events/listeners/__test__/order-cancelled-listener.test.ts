import mongoose from "mongoose";
import { OrderCancelledListener } from "../order-cancelled-listener";
import { natsWrapper } from "../../../nats-wrapper";
import { Order } from "../../../models/orders";
import { OrderStatus, OrderCancelledEvent } from "@byte_b/common";
import { Message } from "node-nats-streaming";

const createFakeEvent = async () => {
  const listener = new OrderCancelledListener(natsWrapper.client);
  const order = Order.build({
    id: mongoose.Types.ObjectId().toHexString(),
    status: OrderStatus.Created,
    userId: mongoose.Types.ObjectId().toHexString(),
    price: 35,
    version: 0,
  });
  await order.save();

  const data: OrderCancelledEvent["data"] = {
    id: order.id,
    expiresAt: new Date().toISOString(),
    status: order.status,
    ticket: {
      id: mongoose.Types.ObjectId().toHexString(),
      price: order.price,
    },
    userId: order.userId,
    version: 1,
  };

  //@ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  };

  return { listener, data, order, msg };
};

it("Acknowledges the message", async () => {
  const { listener, data, msg } = await createFakeEvent();
  await listener.onMessage(data, msg);
  expect(msg.ack).toHaveBeenCalled();
});

it("Updates the status of order", async () => {
  const { listener, data, msg, order } = await createFakeEvent();
  await listener.onMessage(data, msg);
  const fetchedOrder = await Order.findById(order.id);
  expect(fetchedOrder!.status).toEqual(OrderStatus.Cancelled);
  expect(fetchedOrder!.price).toEqual(order.price);
});
