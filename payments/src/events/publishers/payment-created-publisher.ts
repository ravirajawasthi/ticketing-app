import { Publisher, PaymentCreatedEvent, Subjects } from "@byte_b/common";

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
  readonly subject = Subjects.PaymentCreated;
}
