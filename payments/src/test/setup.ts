import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import jwt from "jsonwebtoken";
import { randomBytes } from "crypto";

declare global {
  namespace NodeJS {
    interface Global {
      getFakeSigninCookie(id?: string, email?: string): string[];
    }
  }
}

jest.mock("../nats-wrapper");
process.env.STRIPE_KEY =
  "sk_test_51H2ycEHMLi3f3d1TpLvUsTY3Y9at2p7dzGI7maI974vIbD1NXfGnOKnDymeQ8qzIf2kLlnNqQbhdT0x6cj5mhVyM00BONI63f0";
let mongo: MongoMemoryServer;
beforeAll(async () => {
  process.env.JWT_KEY = "asdasdljkhjk";
  mongo = new MongoMemoryServer();
  const mongoURI = await mongo.getUri();
  await mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
});

beforeEach(async () => {
  jest.clearAllMocks();
  const collections = await mongoose.connection.db.collections();
  for (let collection of collections) {
    collection.deleteMany({});
  }
});

afterAll(async () => {
  mongoose.connection.close();
});

global.getFakeSigninCookie = (id?: string, email?: string) => {
  //buid a jwt {id, email}
  const jwtPayload = {
    id: id ? id : new mongoose.Types.ObjectId(),
    email: email
      ? email
      : `${randomBytes(5).toString("hex")}@${randomBytes(5).toString(
          "hex"
        )}.com`,
  };
  //build session object jwt = {my_jwt}
  const jwtString = jwt.sign(jwtPayload, process.env.JWT_KEY!);
  const session = { jwt: jwtString };
  const sessionJSON = JSON.stringify(session);
  //turn session into base 64
  const b64EncodedString = Buffer.from(sessionJSON).toString("base64");
  //return string with encoded data
  return [`express:sess=${b64EncodedString}`]; // we added express:sess because cookie-session wants this format
};
