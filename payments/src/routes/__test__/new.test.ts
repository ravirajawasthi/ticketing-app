import mongoose from "mongoose";
import request from "supertest";
import app from "../../app";
import { Order } from "../../models/orders";
import { OrderStatus } from "@byte_b/common";
import stripe from "../../stripe";
import { Payment } from "../../models/payments";

it("400 Error when Invalid orderId is provided", async () => {
  await request(app)
    .post("/api/payments")
    .set("Cookie", global.getFakeSigninCookie())
    .send({ token: "", orderId: mongoose.Types.ObjectId().toHexString() })
    .expect(400);
});

it("404 Error when trying to pay for a order that does not exist", async () => {
  await request(app)
    .post("/api/payments")
    .set("Cookie", global.getFakeSigninCookie())
    .send({
      token: "dfsafasfd",
      orderId: mongoose.Types.ObjectId().toHexString(),
    })
    .expect(404);
});
it("401 Error when Order doesn't belong to user", async () => {
  const randomUserId = mongoose.Types.ObjectId().toHexString();
  const order = Order.build({
    id: mongoose.Types.ObjectId().toHexString(),
    price: 3124,
    status: OrderStatus.Created,
    userId: randomUserId,
    version: 0,
  });
  await order.save();

  await request(app)
    .post("/api/payments")
    .set("Cookie", global.getFakeSigninCookie())
    .send({
      token: "pamkfgyvb",
      orderId: order.id,
    })
    .expect(401);
});
it("400 Error when Order is already cancelled", async () => {
  const randomUserId = mongoose.Types.ObjectId().toHexString();
  const order = Order.build({
    id: mongoose.Types.ObjectId().toHexString(),
    price: 3124,
    status: OrderStatus.Cancelled,
    userId: randomUserId,
    version: 0,
  });
  await order.save();
  await request(app)
    .post("/api/payments")
    .set("Cookie", global.getFakeSigninCookie(randomUserId))
    .send({
      token: "pamkfgyvb",
      orderId: order.id,
    })
    .expect(400);
});

it("returns 201 in case of successful payments", async () => {
  const randomUserId = mongoose.Types.ObjectId().toHexString();
  const price = Math.floor(Math.random() * 10000);
  const order = Order.build({
    id: mongoose.Types.ObjectId().toHexString(),
    price,
    status: OrderStatus.Created,
    userId: randomUserId,
    version: 0,
  });
  await order.save();

  await request(app)
    .post("/api/payments")
    .set("Cookie", global.getFakeSigninCookie(randomUserId))
    .send({
      token: "tok_visa",
      orderId: order.id,
    })
    .expect(201);
  const stripeCharges = await stripe.charges.list({ limit: 10 });
  const stripeCharge = stripeCharges.data.find((charge) => {
    return charge.amount === price * 100;
  });

  expect(stripeCharge).toBeDefined();
  expect(stripeCharge?.currency).toEqual("inr");
  const payment = await Payment.findOne({
    orderId: order.id,
    stripeId: stripeCharge?.id,
  });
  expect(payment).not.toBeNull();
});
