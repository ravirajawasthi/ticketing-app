import express, { Response, Request } from "express";
import mongoose from "mongoose";
import { body } from "express-validator";
import {
  requireAuth,
  validateRequest,
  NotFoundError,
  NotAuthorizedError,
  OrderStatus,
  BadRequestError,
} from "@byte_b/common";
import { Order } from "../models/orders";
import stripe from "../stripe";
import { Payment } from "../models/payments";
import { PaymentCreatedPublisher } from "../events/publishers/payment-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/payments",
  requireAuth,
  [
    body("token").not().isEmpty().withMessage("Bad Request"),
    body("orderId").not().isEmpty().withMessage("Bad Request"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { token, orderId } = req.body;
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
      throw new BadRequestError("Invalid OrderID provided");
    }
    const order = await Order.findById(orderId);
    if (!order) {
      throw new NotFoundError();
    }
    if (order.userId !== req.currentUser!.id) {
      //currentUser will be defined by requireAuth middleware
      throw new NotAuthorizedError();
    }

    if (order.status === OrderStatus.Cancelled) {
      throw new BadRequestError("❌ Order is already expired");
    }

    const fetchedPayment = await Payment.findOne({ orderId });
    if (!fetchedPayment) {
      const charge = await stripe.charges.create({
        currency: "inr",
        amount: order.price * 100,
        source: token,
        description: "DEVELOPMENT_TEST",
      });
      const payment = Payment.build({
        orderId: orderId,
        stripeId: charge.id,
      });
      await payment.save();
      await new PaymentCreatedPublisher(natsWrapper.client).publish({
        id: payment.id,
        orderId: payment.orderId,
        stripeId: charge.id,
      });
      res.status(201).send({ id: payment.id });
    } else {
      throw new BadRequestError(
        "❌ Cannot pay for same order twice. Your card will not be charged"
      );
    }
  }
);

export { router as createChargeRouter };
