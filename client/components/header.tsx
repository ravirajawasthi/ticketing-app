import { useContext } from 'react';
import Link from 'next/link';
import UserAuthContext from '../contexts/UserAuth.context';
const Header: React.FunctionComponent = (_props) => {
  const { currentUser } = useContext(UserAuthContext.Context);
  const links = [
    { label: 'Signup', href: '/auth/signup', userStatus: false },
    { label: 'Signin', href: '/auth/signin', userStatus: false },
    { label: 'Signout', href: '/auth/signout', userStatus: true },
  ];
  const linkMarkup = links.map(
    ({ userStatus, label, href }) =>
      userStatus === Boolean(currentUser) && (
        <li key={href} className="nav-item">
          <Link href={href}>
            <a className="nav-link">{label}</a>
          </Link>
        </li>
      )
  );
  return (
    <nav className="navbar navbar-light bg-light">
      <Link href="/">
        <a className="navbar-brand">GitTix</a>
      </Link>
      <div className="d-flex justify-content-end">
        <ul className="nav d-flex align-items-center">{linkMarkup}</ul>
      </div>
    </nav>
  );
};

export default Header;
