import axios, { AxiosResponse } from "axios";
import { Method } from "../interfaces/index";
import { useState } from "react";
import { ServerError } from "../interfaces";

const useRequest = (
  url: string,
  method: Method,
  body: any,
  callback?: (result: AxiosResponse) => void
) => {
  const [errors, setErrors] = useState<JSX.Element | null>(null);
  const doRequest = async (args = {}) => {
    body = { ...body, ...args };
    try {
      setErrors(null);
      let response: AxiosResponse;
      switch (method) {
        case "post":
        case "POST":
          response = await axios.post(url, body);
          break;
        case "get":
        case "GET":
          response = await axios.get(url, body);
      }
      if (callback) {
        callback(response);
      }
      return response.data;
    } catch (err) {
      const errors: ServerError = err.response.data.errors;
      setErrors(
        <div className="alert alert-danger">
          <h4>Oh no😯😯🤷‍♂️🤷‍♀️</h4>
          <ul>
            {errors.map((err) => (
              <li key={err.message}>{err.message}</li>
            ))}
          </ul>
        </div>
      );
    }
  };
  return { doRequest, errors };
};

export default useRequest;
