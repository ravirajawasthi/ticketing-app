import { NextPage } from "next";
import Router from "next/router";
import { useState } from "react";
import useRequest from "../../hooks/useRequest";

const NewTicket: NextPage = () => {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");

  const { doRequest, errors } = useRequest(
    "/api/tickets",
    "POST",
    { title, price },
    () => {
      Router.push("/");
    }
  );

  const formatPrice = () => {
    const value = parseFloat(price);
    if (isNaN(value)) {
      return;
    }
    setPrice(value.toFixed(2));
  };

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    await doRequest();
  };
  return (
    <div className="container">
      <h1>Create new Ticket</h1>
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label>Title</label>
          <input
            className="form-control"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label>Title</label>
          <input
            className="form-control"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            onBlur={formatPrice}
          />
        </div>
        <button className="btn btn-primary">Submit</button>
        <div className="mt-3">{errors}</div>
      </form>
    </div>
  );
};

export default NewTicket;
