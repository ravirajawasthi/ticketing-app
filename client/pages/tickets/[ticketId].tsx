import buildClient from "../../api/build-client";
import { order, ticket } from "../../interfaces/index";
import { NextPage } from "next";
import useRequest from "../../hooks/useRequest";
import Router from "next/router";

interface IProps {
  ticket: ticket;
}

const TicketShow: NextPage<IProps> = ({ ticket }) => {
  const { doRequest, errors } = useRequest(
    "/api/orders",
    "POST",
    { ticketId: ticket.id },
    (response) => {
      const order: order = response.data;
      Router.push("/orders/[orderId]", `/orders/${order.id}`);
    }
  );
  return (
    <div className="container">
      <h1>{ticket.title}</h1>
      <h4>Price: {ticket.price}</h4>
      <button onClick={() => doRequest()} className="btn btn-primary">
        Purchase!
      </button>
      <div className="mt-3">{errors}</div>
    </div>
  );
};

TicketShow.getInitialProps = async (context) => {
  const AxiosClient = buildClient(context);
  const { ticketId } = context.query;
  const { data } = await AxiosClient.get(`/api/tickets/${ticketId}`);
  return { ticket: data };
};

export default TicketShow;
