import StripeCheckout from "react-stripe-checkout";
import buildClient from "../../api/build-client";
import { order } from "../../interfaces";
import { NextPage } from "next";
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../../contexts/UserAuth.context";
import useRequest from "../../hooks/useRequest";
import Router from "next/router";

interface IProps {
  order: order;
}

const OrderShow: NextPage<IProps> = ({ order }) => {
  const [timeLeft, setTimeLeft] = useState(0);
  const User = useContext(UserContext);
  const { doRequest, errors } = useRequest(
    "/api/payments",
    "POST",
    { orderId: order.id },
    (_response) => Router.push("/")
  );
  useEffect(() => {
    const findTimeLeft = () => {
      const msLeft = new Date(order.expiresAt).getTime() - new Date().getTime();
      setTimeLeft(Math.round(msLeft / 1000));
    };
    findTimeLeft();
    const intervalId = setInterval(findTimeLeft, 1000);
    return () => {
      clearInterval(intervalId);
    };
  }, [order]);
  if (timeLeft < 0) {
    return <div>Order Expired!</div>;
  }
  return (
    <div>
      <h3 className="container">{timeLeft} seconds left to complete order</h3>
      <StripeCheckout
        token={async (token) => await doRequest({ token: token.id })}
        stripeKey="pk_test_51H2ycEHMLi3f3d1TLN9BS6g6qflxgkGS5cUtW9ekCnK3yp2578Sq4QPp7xNFG20BgVXdRKfMKGsSJ5D5qWs0R2CW00XB5gxlyO"
        amount={order.ticket.price * 100}
        email={User.getUser()?.email}
        currency="INR"
      />
      <div className="mt-3">{errors}</div>
    </div>
  );
};

OrderShow.getInitialProps = async (context) => {
  const { orderId } = context.query;
  const AxiosClient = buildClient(context);
  const response = await AxiosClient.get(`/api/orders/${orderId}`);
  return { order: response.data };
};

export default OrderShow;
