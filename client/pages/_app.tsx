import "bootstrap/dist/css/bootstrap.min.css";
import Header from "../components/header";
import UserContext from "../contexts/UserAuth.context";
import buildClient from "../api/build-client";
import { AppContext, AppProps } from "next/app";

const AppComponent = ({
  Component, //Component to be rendered
  pageProps, //Props returned by getInitialProps of component
  currentUser, //CurrentUserResponse
}: AppProps & any) => {
  return (
    <UserContext.Provider currentUser={currentUser}>
      <Header />
      <Component {...pageProps} />
    </UserContext.Provider>
  );
};

AppComponent.getInitialProps = async (context: AppContext) => {
  const AxiosClient = buildClient(context.ctx);
  const response = await AxiosClient.get("/api/users/currentuser");
  const currentUser = response.data.currentUser;
  let pageProps;
  if (context.Component.getInitialProps) {
    pageProps = await context.Component.getInitialProps(context.ctx);
  }
  return { currentUser, pageProps };
};

export default AppComponent;
