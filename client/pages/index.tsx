import { useContext } from "react";
import { NextPage } from "next";
import UserContext from "../contexts/UserAuth.context";
import buildClient from "../api/build-client";
import Link from "next/link";
import { tickets } from "../interfaces/index";
interface IProps {
  tickets: tickets;
}

const LandingPage: NextPage<IProps> = ({ tickets }) => {
  const ticketList = tickets.map((ticket) => (
    <tr key={ticket.id}>
      <td>{ticket.title}</td>
      <td>{ticket.price}</td>
      <td>
        <Link href="/tickets/[ticketId]" as={`/tickets/${ticket.id}`}>
          <a>View</a>
        </Link>
      </td>
    </tr>
  ));
  const currentUser = useContext(UserContext.Context);
  return (
    <div className="container">
      <h1>Tickets</h1>
      <table className="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Price</th>
            <th>Link</th>
          </tr>
        </thead>
        <tbody>{ticketList}</tbody>
      </table>
    </div>
  );
};

LandingPage.getInitialProps = async (context) => {
  const client = buildClient(context);
  const { data } = await client.get("/api/tickets");
  return { tickets: data };
};

export default LandingPage;
