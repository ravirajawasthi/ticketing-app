import useRequest from '../../hooks/useRequest';
import { useEffect } from 'react';
import { NextPage } from 'next';

const Signout: NextPage = (_props) => {
  const { doRequest } = useRequest(
    '/api/users/signout',
    'post',
    {},
    () => (window.location.href = '/')
  );
  useEffect(() => {
    doRequest();
  }, []);
  return <div>Signing out...</div>;
};

export default Signout;
