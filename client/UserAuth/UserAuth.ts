import { CurrentUserResponse } from "../interfaces";
class UserAuth {
  protected authenticated: boolean = false;
  protected currentUser: CurrentUserResponse = null;
  isAuthenticated() {
    return this.authenticated;
  }
  logout() {
    this.authenticated = false;
    this.currentUser = null;
  }
  login(response: CurrentUserResponse) {
    if (response === null) return;
    this.authenticated = true;
    this.currentUser = {
      email: response.email,
      iat: response.iat,
      id: response.id,
    };
  }
  getUser() {
    return this.currentUser;
  }
}

export default UserAuth;
