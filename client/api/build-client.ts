import Axios from 'axios';
import { GetServerSidePropsContext } from 'next';
import { ParsedUrlQuery } from 'querystring';
import { NextPageContext } from 'next/dist/next-server/lib/utils';

export default (
  context: GetServerSidePropsContext<ParsedUrlQuery> | NextPageContext
) => {
  if (typeof window === 'undefined') {
    // we are on server
    return Axios.create({
      baseURL:
        'http://ingress-nginx-controller.ingress-nginx.svc.cluster.local',
      headers: context.req?.headers,
    });
  } else {
    return Axios.create({
      baseURL: '/',
    });
  }
};
