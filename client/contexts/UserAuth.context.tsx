import { createContext } from "react";
import { CurrentUserResponse } from "../interfaces";
import { NextPage } from "next";
import UserAuth from "../UserAuth/UserAuth";

interface IProps {
  currentUser: CurrentUserResponse;
}

export const UserContext = createContext(new UserAuth());

const UserContextProvider: NextPage<IProps> = ({ currentUser, children }) => {
  const User = new UserAuth();
  User.login(currentUser);

  return <UserContext.Provider value={User}>{children}</UserContext.Provider>;
};
export default { Context: UserContext, Provider: UserContextProvider };
