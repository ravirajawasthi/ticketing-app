import { OrderStatus } from "@byte_b/common";
export type User = {
  id: number;
  name: string;
};

export type ServerError = {
  message: string;
  field?: string;
}[];

export type Method = "get" | "GET" | "post" | "POST";

export interface CurrentUser {
  id: string;
  email: string;
  iat: number;
}

export type CurrentUserResponse = CurrentUser | null;

export type ticket = {
  userId: string;
  id: string;
  version: number;
  price: number;
  title: string;
};

export type order = {
  expiresAt: string;
  id: string;
  status: OrderStatus;
  userId: string;
  version: number;
  ticket: {
    _id: string;
    price: number;
    title: string;
    version: number;
  };
};

export type tickets = ticket[];
